from browser import doc
from browser.html import *

trans_menu = { 
    'menu_console':{'en':'Console'},
    'menu_editor':{'en':'Editor'},
    'menu_gallery':{'en':'Gallery'},
    'menu_doc':{'en':'Documentation'},
    'menu_download':{'en':'Download'},
    'menu_dev':{'en':'Development'},
    'menu_groups':{'en':'Groups'}
}
links = {'home':'index.html',
    'console':'tests/console1.html',
    'editor':'tests/editor.html',
    'gallery':'gallery/gallery_%s.html',
    'doc':'doc/%s/index.html',
    'download':'https://bitbucket.org/olemis/brython/downloads',
    'dev':'https://bitbucket.org/olemis/brython/src',
    'groups':'groups.html'
}

def show(prefix=''):
    # detect language
    language = "en" # default
    has_req = False
    
    qs_lang = doc.query.getfirst("lang")
    if qs_lang and qs_lang in ["en","fr","es","pt"]:
        has_req = True
        language = qs_lang
    else:
        import locale
        try:
            lang,enc = locale.getdefaultlocale()
            lang = lang[:2]
            if lang in ["en","fr","es","pt"]:
                language = lang
        except:
            pass

    _banner = doc['banner_row']
    
    for key in ['home','console','editor','gallery','doc','download','dev','groups']:
        if key in ['download','dev']:
            href = links[key]
        else:
            href = prefix+links[key]
        if key in ['doc','gallery']:
            href = href %language
        if has_req and key not in ['download','dev']:
            # add lang to href
            href += '?lang=%s' %language
        if key == 'home':
            img = IMG(src=prefix+"brython_white.png",Class="logo")
            link = A(img,href=href)
            cell = TD(link,Class="logo")
        else:
            link = A(trans_menu['menu_%s'%key][language],href=href,Class="banner")
            cell = TD(link)
        if key in ['download','dev']:
            link.target = "_blank"        
        _banner <= cell

    return qs_lang,language